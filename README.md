# E1.73 (Uniform Device Representation)

This repository contains supporting files for the BSR E1.73 family, a series of draft ANSI standards for uniform program-readable descriptions of controllable devices designed for entertainment technology, building automation, and related industries.

The standard is in active development by the [ESTA](https://tsp.esta.org/tsp/index.html) [Control Protocols Working Group](https://tsp.esta.org/tsp/working_groups/CP/cp.html). There is currently no public draft available. To collaborate on the draft standards and get more context on the contents of this repository, please [join](https://tsp.esta.org/tsp/working_groups/index.html) the CPWG.

## Structure

The project is structured such that each stage of development is contained within a directory. This allows the team to make continual improvements and fixes to the schemas in a previosuly published revision.

Draft and ratified documents are identified using a number, for example `CP/2023-1007` which can be found on the cover page of the document. This number has a direct pairing with a directory within this project.

| Document     | Stage            | Number          |                                                                                      |
| -------------| ---------------- | --------------- | ------------------------------------------------------------------------------------ |
| BSR E1.73-1  | Public Review 1  | CP/2023-1007    | [schemas/draft-2023-1](/schemas/draft-2023-1)                                        |
| BSR E1.73-1  | Draft            | -               | [schemas/draft-2024-1](/schemas/draft-2024-1)                                        |
| BSR E1.73-2  | Public Review 1  | CP/2023-1008    | [libraries/core/draft-2023-1](/libraries/core/draft-2023-1)                          |
| BSR E1.73-2  | Draft            | -               | [libraries/core/draft-2024-1](/libraries/core/draft-2024-1)                          |
| BSR E1.73-3  | Public Review 1  | CP/2023-1009    | [libraries/intensity-color/draft-2023-1](/libraries/intensity-color/draft-2023-1)    |
| BSR E1.73-3  | Draft            | -               | [libraries/intensity-color/draft-2024-1](/libraries/intensity-color/draft-2024-1)    |
| BSR E1.73-4  | Public Review 1  | CP/2023-1010    | [libraries/motion/draft-2023-1](/libraries/motion/draft-2023-1)                      |
| BSR E1.73-4  | Draft            | -               | [libraries/motion/draft-2024-1](/libraries/motion/draft-2024-1)                      |

## Development

Contribute to the schemas and libraries by opening merge requests against the `main` branch. Libraries are automatically validated against the corresponding document schema by CI.

Files contained within the `libraries` directory which do not form part of the BSR E1.73 schema must be prefixed with `_`.
