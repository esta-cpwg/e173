# Improving Identification Constructs within E1.73

- Author: Sam Kearney
- Date: 2024-04-14

## Abstract

The concepts 'Fully Qualified Identifier', 'Versioned Qualified Identifier', 'Versioned Fully Qualified Identifier', and 'Relative Identifier' should be removed from BSR E1.73. These concepts form part of an underspecified URI-like syntax which unnecessarily complicates parsing and validation of UDR documents, breaks layering with the upcoming next-gen transport standard, and causes confusion by not being consistent with the usage of URIs and reverse domain names elsewhere.

This document outlines in detail the reasons for the removal of these concepts and their proposed replacements.

## Background

Section 5 of BSR E1.73-1 defines our current approach to identification of most entities within the standard. To summarize the current defined terms and their meanings:

### Organization Identifier

An **Organization Identifier** in reverse-domain-name notation, e.g. `org.esta`, is used to uniquely identify an organization or user and namespace other entities under that organization or user. Organizations use a domain name that they have control over, and users use a special form of this identifier called an **End User Identifier** of the form `org.esta.e173.user.<uuid>`.

```
org-id      = reg-name / user-id ; See [URI] for the definition of "reg-name".

user-id     = "org" delimiter "esta" delimiter "e173" delimiter "user" delimiter uuid

uuid        = UUID  ; See [UUID], Section 3 for the full definition of UUID

delimiter   = %x2E  ; "."
```

### Qualified Identifier

A **Qualified Identifier** is an identifier for a library, device class, and system which is qualified by an Organization Identifier. An example is `org.esta.lib.intensity-color`, which identifies the ESTA Intensity/Color Library.

```
qualified-id    = org-id delimiter ("lib" / "dev" / "sys") delimiter identifier

identifier      = %x25-2D / %x30-39 / %x3C-3E / %x41-5A / %x5E-D7FF / %xE000-10FFFF
```

### Versioned Qualified Identifier

A **Versioned Qualified Identifier** is a Qualified Identifier which is further qualified by a specific version of the entity it is identifying. An example is `com.example.dev.superlight#2.0.3`, which may identify version 2.0.3 of the `superlight` device maintained by the `example.com` organization.

```
versioned-qualified-id  = qualified-id version-delimiter version-number
                            ; "version-number" is a 3-digit version format which conforms to semver, omitted for brevity

version-delimiter       = %x23  ; "#"
```

### Fully Qualified Identifier

A **Fully Qualified Identifier** is an identifier for an Atomic Data Object (a Parameter, Structure, Serializer or Resource), which is qualified with a Qualified Identifier to fully identify and deduplicate it. A Fully Qualified Identifier begins with a Qualified Identifier and appends the ID of an Atomic Data Object.

```
fully-qualified-id  = qualified-id path-separator (par-type-class-id / str-type-class-id / ser-type-class-id / res-type-class-id)

par-type-class-id   = "par" path-separator par-class-id
                       ; See Section 8.1.1 for the definition of "par-class-id".

str-type-class-id   = "str" path-separator str-class-id
                       ; See Section 9.1.1 for the definition of "str-class-id".

ser-type-class-id   = "ser" path-separator ser-class-id
                       ; See Section 10.1.1 for the definition of "ser-class-id".

res-type-class-id   = "res" path-separator res-class-id
                       ; See Section 11.1.1 for the definition of "res-class-id".

path-separator      = %x2F  ; "/"
```

The `class-id` types generally conform to `identifier` as described above, with a few caveats that are omitted here for brevity.

### Versioned Fully Qualified Identifier

A **Versioned Fully Qualified Identifier** is like a Fully Qualified Identifier, except it begins with a Versioned Qualified Identifier instead of a Qualified Identifier, for further deduplication.

### Relative Identifier

A **Relative Identifier** is a way of locating an Atomic Data Object within the same Library, Device Class, or System. Despite the name, relative identifiers are essentially absolute within their Library, Device Class, or System; they begin with a `/` and identify an ADO from the 'top' of the entity.

```
relative-id	= path-separator (par-type-class-id / str-type-class-id / ser-type-class-id / res-type-class-id)
```

## Prior Art

The design for the system of identifiers that we currently have in the document takes inspiration from many already established standards and schemes.

### Reverse Domain Name Notation

The use of [reverse domain name notation](https://en.wikipedia.org/wiki/Reverse_domain_name_notation) for Organization Identifiers follows a convention established by many software systems. Some of the most prominent ones are listed below.

#### Apple UTIs

Apple uses a concept called a [Uniform Type Identifier](https://en.wikipedia.org/wiki/Uniform_Type_Identifier) (UTI) to identify classes or types of items within their operating systems, including macOS and iOS. UTIs are used to identify many classes of entities, including MIME types, file types, and third-party apps.

A UTI identifier [is defined as](https://developer.apple.com/documentation/uniformtypeidentifiers/defining_file_and_data_types_for_your_app) a reverse DNS identifier that contains only alphanumeric characters (A-Z, a-z, 0-9), hyphens (-) and periods (.). An example is `com.example.greatAppDocument`.

#### Java

The Java programming language uses [qualified names](https://docs.oracle.com/javase/specs/jls/se10/html/jls-6.html#jls-6.2) to refer to program entities. By strong convention, qualified names in Java follow a reverse-domain-name-style hierarchy with hierarchical components separated by `.` characters. Convention [also dictates](https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html) that companies should begin these identifiers with the domain name they control, e.g. `com.example.graphics.Rectangle`.

Implementations of Java [commonly map](https://docs.oracle.com/javase/tutorial/java/package/managingfiles.html) these identifiers to directories in a filesystem; for example, when encountering the import name `com.example.graphics.Rectangle`, the implementation would look for a file called `.../com/example/graphics/Rectangle.java`, relative to some established path.

Each component of a Java qualified name is an [identifier](https://docs.oracle.com/javase/specs/jls/se10/html/jls-3.html#jls-3.8), which allows a wide set of letter- and digit-like Unicode characters consistent with many other modern programming languages.

### URI

[RFC 3986](https://www.rfc-editor.org/rfc/rfc3986) is the most recent Internet Standard to define the URI, a sequence of characters that identifies a resource. URIs are similar to, and a superset of, URLs, which _locate_ resources using a protocol such as HTTP.

URIs have a standard syntax defined in section 3 of RFC 3986; in addition to the standard syntax, URIs delegate to _schemes_ to further restrict and apply semantics to their syntax. For example, any URI can be parsed up to a point according to the standard rules defined in RFC 3986; URIs beginning with `http:` have further restrictions and semantics defined by the HTTP/2 and HTTP/3 protocols.

```
URI         = scheme ":" hier-part [ "?" query ] [ "#" fragment ]

hier-part   = "//" authority path-abempty
            / path-absolute
            / path-rootless
            / path-empty
```

Further syntax definition of the portions of a URI can be found in RFC 3986 section 3.

## Goals

At this point, it's worth taking a step back to consider the goals we are trying to achieve related to identification within the E1.73 family of standards. E1.73 contains two classes of entities that need to be identified:

1. Top-level objects, comprised of Libraries, Device Classes, and Systems (by the way, I'm using 'top-level object' to refer to these three entities which can be found at the top level of a UDR document; but do we already have a standard name for them? I couldn't find one.)
2. All other entities, comprised of Parameter Classes, Parameters, Structure Classes, Structures, Serializer Classes, Serializers, Resource Classes, Resources, and Devices

Entities in class 2 above need to be identified in two possible contexts:

1. **Relative to an implicit top-level object**. This is used when identifying an entity in a context where the top-level object containing the entity is already known or does not need to be specified.
2. **Relative to an explicit top-level object**. This is used when identifying an entity in a context where the top-level object containing the entity needs to be specified.

Orthogonal to this, there are also the following two options for identification contexts which apply to identifying all entities, including top-level objects:

1. **Concrete identification context** means an instance where we need to identify entities from somewhere within the E1.73 standard.
2. **Abstract identification context** means an instance where we loosely define the way an entity _could_ be identified from _outside_ the E1.73 standard.

### Identification Stories: Concrete

1. Structure instances need to reference Parameters, Structures, Serializers and Resources defined within the same Device Class (BSR E1.73-1 §9.2.3).
   - Structure instances need to reference portions of another Structure rather than the whole thing (BSR E1.73-1 §9.2.3.2)
2. Serializer instances need to reference Parameters, Structures, Serializers, and Resources defined within the same Device Class (BSR E1.73-1 §10.2.3).
   - Serializer instances need to reference portions of a Structure rather than the whole thing (BSR E1.73-1 §10.2.3.2)
3. Serializer instances need to reference Structure instances as part of the 'Supported Structures' functionality (BSR E1.73-1 §10.2.4.3).
4. Device instances and System overlays need to provide static values for Parameters, Structures and Resources defined within a Device Class. To do this, they need to reference those Parameters, Structures and Resources. (BSR E1.73-1 §12.2.2.8.1, 12.2.3, 13.2.5)
5. Systems need to define Containers which define logical groupings of device instances. These Containers need to reference Device instances that are contained inside them (BSR E1.73-1, §13.2.2)
6. Systems need to define Locations in space which Devices maybe placed at. These Locations need to reference Device instances that are positioned at that location (BSR E1.73-1, §13.2.3)
   - System Locations may include Controller objects which reference Device instances (e.g. motors) which provide a dynamic position or rotation offset for that Location. (BSR E1.73-1, §13.2.3.1)
7. Systems need to define Locations which identify points of interest within a venue. These Locations need to reference Device instances to indicate which devices are 'focused' at this location. (BSR E1.73-1, §13.2.4)
8. Each instance of a Parameter, Structure, Serializer and Resource needs to reference its respective Class (many sections in BSR E1.73-1). This referencing takes two possible forms:
   - Referencing a Class defined in a Library (external to the Device Class that the instance appears in).
   - Referencing a Class defined in a Device Library (internal to the Device Class that the instance appears in).
9. Overlay Localizations need to identify localization strings within external Libraries or Device Classes to provide overlay localizations for (BSR E1.73-1 §14.2.2)
10. Libraries and Device Classes need to be identified by a 2-tuple of their Qualified Identifier and version in several cases:
    - When being defined in a UDR Document
    - When being referenced in a UDR Archive (Device Classes only)

### Identification Stories: Abstract

1. Libraries, Parameter Classes, Parameters, Structure Classes, Structures, Serializer Classes, Serializers, Resource Classes, Resources, and Device Classes need to be identified when retrieved dynamically from Components. (BSR E1.73-1 §17.2)
2. Libraries and Device Classes should be able to be globally uniquely identified when held in some external database or stored alongside software that uses them.

## Issues with the Existing Concepts

The current identification concepts work, but they have some design issues which may lead to problems with adoption and interoperability of this standard in the future. Fortunately, these issues have solutions which have the added bonus of simplifying the standard.

### Breaking Layering

In many cases, the current standard draft attempts to reach 'up' beyond the standard and specify how its concepts should be referenced from external protocols or constructs. In BSR E1.73-1 §17.2, Table 17-1 specifies (among other things) the type and format of identifier which should be used to "request an Entity from a Component." In practice, this generally means the identifier that is used to perform runtime fetching of information about entities like Parameters and Structures from a device. The document specifies in normative language that the identifier format defined in this document, e.g. a Versioned Qualified Identifier such as `com.example.dev.superlight#2.0.0`, is used to retrieve information about an entity.

The problem with this is that it overspecifies the method of accessing entity information. For example, it is only necessary to say that a Device Class is identified uniquely by the 2-tuple of a Qualified Identifier in format A and a version in format B, without specifying a syntax that contatenates them together with the `#` symbol. Specifying more loosely like this leaves a lot more flexibility for transport protocol implementations, which may wish to (for example) transmit the Qualified Identifier and version in separate length-delimited fields, rather than as a single string, for ease of parsing or to fit into the semantics of a protocol.

Another example of this type of overspecification, which is not within the scope of this RFC but may similarly need to be clarified or fixed, is the syntax for identifying specific indices of a multiply-instantiated Parameter using the 'Count' field (BSR E1.73-1 §8.2.3.5). This section specifies that, for example, a parameter named `intensity` which is instantiated 3 times _shall_ be identified using a bracket-delimited indexing syntax, e.g.

```
intensity[0]
intensity[1]
intensity[2]
```

This syntax is useful and necessary when referencing Parameters from other constructs within the E1.73 standard, such as Structures which reference Parameters within the same Device Class. But, similar to the above, an external protocol might find it more convenient to use two fields, one with the string identifier of the Parameter, and another with the index in raw integer format.

To sum up, we should try our best in E1.73 to specify the various entities in the object model, how they are related, and what information is needed to uniquely identify them, while stopping short of a fully-specified identification syntax, to allow more flexibility for future protocols and standards which build on this one. This has the added benefit of simplifying the standard (see below).

#### Incompatibility with URIs

A special case of the layering issue described above is that our identification syntax is incompatible with the URI format described in RFC 3986. We make no claim that the identification syntax is URI-compatible, but I still contend that the fact that it is not compatible will cause problems.

Some examples of possible incompatibility issues between existing E1.73 identifier syntax and the URI format:

- There are some reserved characters and character rules which are incompatible with the URI standard. For example:
  - URIs only allow alphanumeric characters (plus a couple of special characters) from the ASCII character set in both the authority and path sections.
    ```
    unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
    ```
    Characters that fall outside this set must be percent-encoded. E1.73 allows many Unicode code points in its identifiers, and makes no requirement to convert them down to ASCII using some mechanism like percent-encoding or Punycode.
  - E1.73 uses the `#` symbol to construct Versioned Qualified Identifiers, but URIs use this character to denote the beginning of the _fragment_ section. Many sections in BSR E1.73-1 show a syntax for referencing an entity preceded by a Versioned Qualified Identifier in this format:
    ```
    com.example.dev.superlight#2.0.3/par/main-intensity
    ```
    If parsed as a URI (putting aside the issue with the missing scheme (see below)), the portion `com.example.dev.superlight` would be parsed as a path with one segment, and `2.0.3/par/main-intensity` would be parsed as the fragment. This is probably not what would be intended _if_ we wanted to make our syntax URI-compatible.
- The standard does not define a _scheme_ for UDR (e.g. `udr:`). All URIs require a scheme that define its semantics. Theoretically, one could be defined in the future.

We may see benefit in making our identification syntax URI-compatible, since URIs are used for many purposes that might benefit users of our standard, such as in API documentation for common protocols such as HTTP and COAP, file type association on operating systems, etc. Libraries that parse URIs are quite common.

But, even if we don't want to make our syntax URI-compatible, it would be confusing to users to define a syntax that _looks like_ URIs but in reality is slightly different. As discussed later, we can accomplish all of our referencing needs without a URI-like syntax, so it would be better to not define one now and leave the door open for defining one that is URI-conformant in the future.

### Overengineering / Excessive Complexity

> "Everything should be made as simple as possible, but no simpler" (commonly attributed to Albert Einstein)

Unnecessary complexity in a standard harms adoptability, because it adds more cognitive effort necessary to understand how to implement the standard, and it harms interoperability by increasing the likelihood of mistakes in implementation. There are many opportunities to reduce complexity by replacing these identification concepts with simpler versions.

#### String Parsing

By defining a multi-part string syntax for identification, we add the necessity for parsing this syntax. The parsing logic needed for the identification strings is not especially complex, but it adds another layer to the code needed to parse and validate a UDR document.

This is unnecessary when you consider the fact that we are already using a grammar which allows delimiting fields with different meanings (JSON) to represent UDR data. For example, consider the following Versioned Qualified Identifier which is meant to represent two distinct pieces of data (a Qualified Identifier and a version):

```
"com.example.dev.superlight#2.0.3"
```

This can trivially be transposed to a JSON object containing two fields:

```json
{
  "id": "com.example.dev.superlight",
  "version": "2.0.3"
}
```

In the Proposed Replacements section below, this type of transposition is the most commonly used method to replace the current identifier syntax.

There are two main benefits of the JSON object syntax in this context:

1. More parsing tasks are delegated to the JSON parsing step, which never requires handwritten code by implementers.
2. More validation tasks are delegated to JSON schema, which similarly does not require handwritten validation code.

#### Unnecessary Hierarchy Concepts

As discussed above, a Relative Identifier identifies an entity within a top-level object such as a Library or Device Class. The use of the term 'relative' implies that there is a flexible hierarchy to navigate up and down within, akin to a filesystem, but in fact this is not the case. Relative Identifiers are actually quite rigid, and this becomes clear when you see that top-level UDR objects are generally quite flat and do not have deep hierarchies at all:

```
Library
+- { Parameter Classes...  }
+- { Structure Classes...  }
+- { Serializer Classes... }
+- { Resource Classes...   }
```

```
Device Class
+- { Parameters...  }
+- { Structures...  }
+- { Serializers... }
+- { Resources...   }
```

```
System
+- { Devices...    }
+- { Containers... }*
+- { Positions...  }*
+- { Locations...  }
+- { Overlays...   }
```

\*Note: These entities do support recursive hierarchy structures by which more of the same type of entity can be defined with a parent-child relationship. However, we currently do not need to reference these entities with Relative Identifiers, and if we do in the future, we would likely map it to JSON-pointer.

Because of this, Relative Identifiers always consist of exactly two segments; a segment that identifies what type of entity it is referencing (e.g. `par` for Parameters, `str` for Structures, etc), and the second segment is the identifier of the entity being referenced.

This means that a hierarchical path structure is unnecessary to represent this kind of identification. All we need is some contextual information about what type of entity is being referenced, and then the entity's identifier.

Sometimes the contextual information is not necessary because there is only one type of entity that is possible in this situation; an example is in the ESTA DMX Serializer (BSR E1.73-2 §8.1), which references Parameters in many places, e.g.

```json
{
  "intensityChunk": {
    "size": 1,
    "offsets": [0],
    "mappingGroups": [
      {
        "mappings": [
          {
            "mappedParam": "/par/main-intens",
            "ranges": [
              {
                "start": 0,
                "end": 1,
                "chunkStart": 0,
                "chunkEnd": 255
              }
            ]
          }
        ]
      }
    ]
  }
}
```

It is trivial to replace `"mappedParam": "/par/main-intens"` here with `"mappedParam": "main-intens"` because:

- Only Parameters are allowed to be referenced here
- Referenced Parameters can only exist in the same Device Class
- Parameters have a flat structure with simple identifiers

A slightly more complex example is `staticValues` in the Device instance, where we have a context where multiple types of entities can be referenced at the same level:

```json
{
  "staticValues": {
    "/par/intensity": 0.5,
    "/res/gobo-1": "gobo-image-1.png",
    "/res/gobo-2": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUg="
  }
}
```

The transposition here is simply to create subgroups for different entity types:

```json
{
  "staticValues": {
    "parameters": {
      "intensity": 0.5
    },
    "resources": {
      "gobo-1": "gobo-image-1.png",
      "gobo-2": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUg="
    }
  }
}
```

Thus, the concept of a Relative Identifier is not necessary, and in most cases when referencing entities within the same top-level object, we can use their simple identifier as defined in BSR E1.73-1 §5.1.

#### Terminology

The current identification scheme requires introducing many terms that sound similar:

- Identifier
- Organization Identifier
- End User Identifier
- Qualified Identifier
- Versioned Qualified Identifier
- Fully Qualified Identifier
- Versioned Fully Qualified Identifier
- Relative Identifier

These names make sense within the internal logic of the standard, but each additional defined term adds cognitive overhead to what is necessary to understand and implement the standard. This by itself is not a reason to change something, but it gives a "plus factor" to any alternative solution which requires defining less terms, as does the one presented in this document.

## Proposed Replacements

This section outlines the proposed changes to the current identification framework, and presents alternative solutions for each of the user stories defined above.

### Identification Changes

Remove the following concepts from Section 5:

- Versioned Qualified Identifier
- Fully Qualified Identifier
- Versioned Fully Qualified Identifier
- Relative Identifier

Reevaluate the ABNF definition of `identifier` since it is now unnecessary to reserve most delimiting characters. In theory, it should be possible to unreserve most characters; or, we could create a reserved/unreserved set which is more compatible with URI. But there is always the possibility of escaping when fitting identifiers into a different context, such as percent-encoding in URIs.

Remove definitions for reserved characters that have no more purpose in section 5.1.

### Concrete Identification Stories

> - Structure instances need to reference Parameters, Structures, Serializers and Resources defined within the same Device Class (BSR E1.73-1 §9.2.3).
> - Serializer instances need to reference Parameters, Structures, Serializers and Resources defined within the same Device Class (BSR E1.73-1 §10.2.3).

In situations where only one entity type is possible, that entity's identifier should be used, e.g.

```json
"mappedParam": "main-intens",
```

In situations where multiple entity types are possible, the Structure or Serializer should be designed such that they are organized into contextual sections, such as:

```json
"parameters": [
   "main-intens",
   "main-shutter"
],
"structures": [
   "esta-dmx"
]
```

> - Structure instances need to reference portions of another Structure rather than the whole thing (BSR E1.73-1 §9.2.3.2)
> - Serializer instances need to reference portions of a Structure rather than the whole thing (BSR E1.73-1 §10.2.3.2)

A two-part syntax should be used, one containing the structure's identifier and another one containing a JSON pointer into the Structure:

```json
{
  "id": "constraints",
  "pointer": "/constraint-groups/0/combinations/1"
}
```

> - Serializer instances need to reference Structure instances as part of the 'Supported Structures' functionality (BSR E1.73-1 §10.2.4.3).

The simple identifier for each Structure should be used, without the `str` prefix.

> - Device instances and System overlays need to provide static values for Parameters, Structures and Resources defined within a Device Class. To do this, they need to reference those Parameters, Structures and Resources. (BSR E1.73-1 §12.2.2.8.1, 12.2.3, 13.2.5)

Divide the 'static values' section into contextual subsections:

```json
{
  "staticValues": {
    "parameters": {
      "intensity": 0.5
    },
    "resources": {
      "gobo-1": "gobo-image-1.png",
      "gobo-2": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUg="
    }
  }
}
```

> - Systems need to define Containers which define logical groupings of device instances. These Containers need to reference Device instances that are contained inside them (BSR E1.73-1, §13.2.2)
> - Systems need to define Locations in space which Devices maybe placed at. These Locations need to reference Device instances that are positioned at that location (BSR E1.73-1, §13.2.3)
>   - System Locations may include Controller objects which reference Device instances (e.g. motors) which provide a dynamic position or rotation offset for that Location. (BSR E1.73-1, §13.2.3.1)
> - Systems need to define Locations which identify points of interest within a venue. These Locations need to reference Device instances to indicate which devices are 'focused' at this location. (BSR E1.73-1, §13.2.4)

The simple identifier for each Device should be used, without the `dev` prefix.

> - Each instance of a Parameter, Structure, Serializer and Resource needs to reference its respective Class (many sections in BSR E1.73-1). This referencing takes two possible forms:
>   - Referencing a Class defined in a Library (external to the Device Class that the instance appears in).
>   - Referencing a Class defined in a Device Library (internal to the Device Class that the instance appears in).

The syntax for each entity instance can be replaced by the following:

```json
{
  "main-intensity": {
    "library": "org.esta.lib.intensity-color",
    "class": "intensity/dimmer"
    // More fields...
  }
}
```

Where `library` can be absent to indicate a reference to a class defined in the Device Library.

> - Overlay Localizations need to identify localization strings within external Libraries or Device Classes to provide overlay localizations for (BSR E1.73-1 §14.2.2)

No change needed - they can continue to reference the Qualified Identifier.

> 10. Libraries and Device Classes need to be identified by a 2-tuple of their Qualified Identifier and version in several cases:
>     - When being defined in a UDR Document
>     - When being referenced in a UDR Archive (Device Classes only)

Replace the Versioned Qualified Identifier with an outer object keyed by the Qualified Identifier and an inner object keyed by the version:

```json
// Old
{
  "org.esta.lib.intensity-color#1.0.0": {
    // Library Document Object
  }
}

// New
{
  "org.esta.lib.intensity-color": {
    "1.0.0": {
      // Library Document Object
    }
  }
}
```

### Abstract Identification Stories

> - Libraries, Parameter Classes, Parameters, Structure Classes, Structures, Serializer Classes, Serializers, Resource Classes, Resources, and Device Classes need to be identified when retrieved dynamically from Components. (BSR E1.73-1 §17.2)

This is out of scope of this standard. The standard should define _what_ exists, the structure, relationships, and data formats of the various entities and objects, but should not define _how_ they are accessed. Section 17.2 should be deleted or drastically simplified.

> - Libraries and Device Classes should be able to be globally uniquely identified when held in some external database or stored alongside software that uses them.

The standard provides all the information necessary to globally identify each of its top-level objects and entities. Databases and software can store this information in a format that is convenient to them.
