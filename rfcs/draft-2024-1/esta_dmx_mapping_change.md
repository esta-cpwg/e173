# Changes to 'Mapping' Object in ESTA DMX Serializer

- Author: Sam Kearney
- Date: 2023-11-28

## Background

One of the core pieces of the ESTA DMX Serializer are Mapping objects, which specify the mapping between certain values of a Parameter and corresponding DMX values. In the basic case, a Mapping object specifies how to map a range of values from a single Parameter to a range of DMX values. For example, in the MAC Encore Performance, this:

![MAC Encore Performance DMX Slot 4](images/mac_encore_performance_dmx_slot_4.png)

Translates to this:

```json
{
  "b4": {
    "size": 1,
    "offsets": [3],
    "mappingGroups": [
      {
        "mappings": [
          {
            "parameter": "/par/cyan",
            "start": 0,
            "end": 1,
            "chunkStart": 0,
            "chunkEnd": 255
          }
        ]
      }
    ]
  }
}
```

There are cases where the values of multiple parameters contribute to a given DMX value. An example is a subset of slot 8 of the MAC Encore Performance:

![MAC Encore Performance DMX Slot 8](images/mac_encore_performance_dmx_slot_8_random_colors.png)

Zooming in on the corresponding 'mappings' object:

```json
{
  "mappings": [
    // ...
    {
      "parameter": "/par/color-effect-program",
      "start": 0,
      "mappings": [
        {
          "parameter": "/par/color-effect-rate",
          "start": 5,
          "chunkStart": 244,
          "chunkEnd": 247
        },
        {
          "parameter": "/par/color-effect-rate",
          "start": 1,
          "chunkStart": 248,
          "chunkEnd": 251
        },
        {
          "parameter": "/par/color-effect-rate",
          "start": 0.5,
          "chunkStart": 252,
          "chunkEnd": 255
        }
      ]
    }
  ]
}
```

For this mapping to take effect, `/par/color-effect-program` must be held at 0. Then, the mappings for `/par/color-effect-rate` apply.

### Current Structure of the 'Mappings' Object

We currently specify that each Mappings object has an optional `mappings` key, which holds an array of child Mappings objects. The parent-child series can be of arbitrary depth, but only the last descendant can have Mappings objects which have actual DMX mappings (i.e. the `chunkStart` and `chunkEnd` keys are present). All parent Mappings object must not have `chunkStart` and `chunkEnd` keys. Therefore, for a hypothetical mapping in which `/par/a` and `/par/b` must be in a specified value or range in order for `/par/c` to be mapped, we would express it as:

```json
{
  "mappings": [
    {
      "parameter": "/par/a",
      "start": 0,
      "mappings": [
        {
          "parameter": "/par/b",
          "start": 1,
          "mappings": [
            {
              "parameter": "/par/c",
              "start": 2,
              "chunkStart": 0,
              "chunkEnd": 50
            }
          ]
        }
      ]
    }
  ]
}
```

## Problem

The parent-child relationship is not ideal for expressing in code, because it requires recursive data structures and recursive functions to parse. Additionally, in general, the relationship that is being expressed by the parent-child relationship is not inherently hierarchical.

For example, the last example given above, expressed in a very condensed form as:

```
U1
+- U2
   +- M1
```

This expresses that unmapped parameters `U1` and `U2` are required to be held in a certain range when the mapping for mapped parameter `M1` is used. The order of `U1` and `U2` could be swapped in the hierarchy and the same information would be expressed.

The core of the relationship that we are ultimately trying to express is that for a given parameter, which has one or more mappings to DMX ranges, one or more other parameters (which are not mapped) must be held in a certain range for the mapping to be valid.

## Solution

The solution is to flatten the mapping object. Each mapping object will specify exactly one _mapped_ parameter, which has one or more mappings. Additionally, a mapping object can contain zero or more _unmapped_ parameters, which only have native value ranges.

The `color-effect-program` example above will become:

```json
{
  "mappings": [
    // ...
    {
      "mappedParam": "/par/color-effect-rate",
      "ranges": [
        {
          "start": 5,
          "chunkStart": 244,
          "chunkEnd": 247
        },
        {
          "start": 1,
          "chunkStart": 248,
          "chunkEnd": 251
        },
        {
          "start": 0.5,
          "chunkStart": 252,
          "chunkEnd": 255
        }
      ],
      "unmappedParams": [
        {
          "parameter": "/par/color-effect-program",
          "start": 0
        }
      ]
    }
  ]
}
```

The three-level example becomes:

```json
{
  "mappings": [
    {
      "mappedParam": "/par/c",
      "ranges": [
        {
          "start": 2,
          "chunkStart": 0,
          "chunkEnd": 50
        }
      ],
      "unmappedParams": [
        {
          "parameter": "/par/a",
          "start": 0
        },
        {
          "parameter": "/par/b",
          "start": 1
        }
      ]
    }
  ]
}
```

The benefits to this approach are:

- Recursive data structures and parsing functions are no longer necessary.
- The 'mapping' object no longer serves two different purposes, simplifying and reducing confusion. Additionally, we avoid repeating the 'mappings' key.
- More constraints can be expressed directly in the JSON schema, as opposed to in text. For example, we will no longer need most of the text expressing that `chunkStart` and `chunkEnd` are mutually exclusive with `mappings`.
- In many cases, we avoid the need to repeat a parameter name many times for multiple contiguous mappings of the same parameter.

See the changes to the schemas and examples for more details.
