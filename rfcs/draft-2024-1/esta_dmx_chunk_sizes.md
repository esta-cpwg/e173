# Remove 'size' field from Chunk object in ESTA DMX Serializer

- Author: Sam Kearney
- Date: 2024-01-11

## Background

Chunk objects in the ESTA DMX serializer describe a DMX "chunk", which is a set of one or more DMX offsets that together form an N-bit value, where N is the number of DMX slots times 8.

The size of a DMX chunk is defined both explicitly by the 'size' field, and implicitly by the length of the array of offsets. Both of these fields are present to enable an optimization where not all of the offsets can be specified, and the rest are extrapolated sequentially:

![Snippet from E1.73-2](images/esta_dmx_offsets_standard_snippet.png)

## Problem

The presence of both 'size' and 'offsets' causes more problems than it solves. It introduces several points of validation that fall outside the capabilities of JSON schema (something we are trying to minimize):

- If the length of the 'offsets' array exceeds 'size', it is an error.
- If 'size' exceeds the length of the offset array, and the offset array has more than one element, the behavior is currently unspecified by the standard. For example:
  ```json
  {
    "size": 3,
    "offsets": [13, 10] // What is the third number inferred to be?
  }
  ```
  Assuming the standard should explicitly declare this invalid, that is another point of validation.

The gain (omitting some offsets from the array) is a micro-optimization that is unnecessary when compared to the typical size of UDR documents. For the benefit of parsing and validation code, it is better to be explicit than implicit.

## Solution

Omit the 'size' field from the chunk object. Infer the size of the chunk from the number of offsets in the 'offsets' array. Require all offsets to be specified in all cases.
