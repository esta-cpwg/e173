# Add support for multiple parameters relating to item in Meta Media

- Author: Dan Murfin
- Date: 2024-04-28

## Background

Items such as labels, images, video a audio are associated with parameters and ranges of values using Meta Media. This is intended to provide a way of describing the information that should be presented on a control surface for a given parameter and value.

## Problem

At present it is only possible to associated a single parameter with a given item in Meta Media. There is a requirement for products such as media servers to be able to associate multiple parameters with a single item.

An example of this is the label for a combination of library and file.

This RFC proposes to extend this to allow multiple parameters to be associated with a single item in Meta Media.

### Current Model

The model below represents a media server with a library and file parameter. The library can be at one of 2 values, as can the file.

It is desirable to specify a label for the file dependent on the library, for example:

Library 0 File 0: Tree  
Library 0 File 1: Bush  
Library 1 File 0: Car  
Library 1 File 1: Truck

```json
{
  "parameters": {
    "/par/library": {
      "labels": [
        {
          "min": 0,
          "@short": "foliage",
          "@long": "foliage_description"
        },
        {
          "min": 1,
          "@short": "vehicle",
          "@long": "vehicle_description"
        }
      ]
    },
    "/par/file": {
      "labels": [
        {
          "min": 0,
          "@short": "tree",
          "@long": "tree_description"
        },
        {
          "min": 1,
          "@short": "bush",
          "@long": "bush_description"
        }
      ]
    }
  }
}
```

This model does not allow for the association of a label with a combination of library and file.

## Solution

It is desirable to keep the model as simple as possible. In addition it is often the case that while one parameter's labels may be associated with the value of another, the reverse is not true. For example, the label for a file may be dependent on the library, but the label for the library is not dependent on the file.

The following solution keeps much of the existing structure which is elegant and allows a logical association of some media with a particular parameter and value, while providing a method to extend this to support other associated parameters.

The core changes are:

- Add an optional `associatedParameters` array to the parameter object. This array contains the identifiers of other parameters that are associated with this parameter.
- Add a `ranges` array to the label (image, video, audio etc.) object. This array contains the ranges of values for the complete set of parameters that this label applies to.

Additional rules:

- If `associatedParameters` is present, then the `ranges` array must be present in the label object (and must not be present if it is also not present).
- The `ranges` array must contain an object for each parameter that the label is associated with. The object must contain the `parameter` identifier and the `min` value (and optionally `max` value) or the `value` for that parameter (as with the existing model).

```json
{
  "parameters": {
    "/par/library": {
      "labels": [
        {
          "min": 0,
          "@short": "foliage",
          "@long": "foliage_description"
        },
        {
          "min": 1,
          "@short": "vehicle",
          "@long": "vehicle_description"
        }
      ]
    },
    "/par/file": {
      "associatedParameters": [
        "/par/library"
      ],
      "labels": [
        {
          "ranges": [
            {
              "parameter": "/par/library",
              "min": 0
            },
            {
              "parameter": "/par/file",
              "min": 0
            }
          ],
          "@short": "tree",
          "@long": "tree_description"
        },
        {
          "ranges": [
            {
              "parameter": "/par/library",
              "min": 0
            },
            {
              "parameter": "/par/file",
              "min": 1
            }
          ],
          "@short": "bush",
          "@long": "bush_description"
        },
        {
          "ranges": [
            {
              "parameter": "/par/library",
              "min": 1
            },
            {
              "parameter": "/par/file",
              "min": 0
            }
          ],
          "@short": "car",
          "@long": "car_description"
        },
        {
          "ranges": [
            {
              "parameter": "/par/library",
              "min": 1
            },
            {
              "parameter": "/par/file",
              "min": 1
            }
          ],
          "@short": "truck",
          "@long": "truck_description"
        }
      ]
    }
  }
}
```


