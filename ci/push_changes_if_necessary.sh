#!/bin/bash

# This script uses a GitLab deploy key to automatically push changes back to the
# default branch of the repo. The public key is stored as a deploy key in the repo
# settings, and the private key is stored as a secure file.
# https://docs.gitlab.com/ee/user/project/deploy_keys/

set -e

if [ -z "${CI}" ]; then
    echo "Script not running from CI. Doing nothing."
    exit 0
fi

if [ "${CI_COMMIT_BRANCH}" != "${CI_DEFAULT_BRANCH}" ]; then
    echo "Not running from default branch. Doing nothing."
    exit 0
fi

curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
chmod 600 .secure_files/id_rsa_deploy_from_ci

mkdir -p ~/.ssh
echo "Host gitlab.com" > ~/.ssh/config
echo "  HostName gitlab.com" >> ~/.ssh/config
echo "  IdentityFile ${PWD}/.secure_files/id_rsa_deploy_from_ci" >> ~/.ssh/config

ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts

git config user.name "GitLab CI"
git config user.email "noreply@gitlab.com"
git remote set-url origin git@gitlab.com:esta-cpwg/e173.git

git add ${CI_PROJECT_DIR}/schemas

if ! git diff-index --quiet HEAD; then
    git commit -m "[skip ci] Update generated files from CI"
    git push -o ci.skip origin HEAD:${CI_COMMIT_BRANCH}
fi
