from jschon import create_catalog, JSON, JSONSchema, URI, LocalSource
from os import listdir, path
from sys import exit

from colorama import init, Fore as fg
init(autoreset=True, strip=False)

## GLOBALS ##
scriptPath = path.dirname(path.realpath(__file__))
repoPath = path.realpath(path.join(scriptPath, ".."))

schemasPath = path.join(repoPath, "schemas")
librariesPath = path.join(repoPath, "libraries")
testsPath = path.join(repoPath, "tests")

errCount = 0

## ALIASSES ##
isDir = path.isdir
isFile = path.isfile

## HELPERS ##
def getSubPaths(p: str) -> list:
    return [path.join(p, f) for f in listdir(p)]

def getLibrariesSubPaths(p: str, version: str) -> list:
    return [path.join(f, version) for f in getSubPaths(p)]

def getTestsSubPath(p: str, version: str) -> str:
    return path.join(p, version)

def containsFile(p: str, f: str) -> bool:
    return path.isfile(path.join(p, f))

def endsWith(p: str, s: str) -> bool:
    return path.basename(p).endswith(s)

def startsWith(p: str, s: str) -> bool:
    return path.basename(p).startswith(s)

def incrementErrorCount():
    global errCount
    errCount += 1

def printStatus(successMessage: str, errorMessage: str, success: bool):
    status_color = fg.GREEN if success else fg.RED
    status_msg = successMessage if success else errorMessage
    prefix = f"{status_color}{'★'*2}{'✦'*2}{'✧'*2}"
    suffix = f"{'✧'*2}{'✦'*2}{'★'*2}"
    print(f"\n{prefix} {status_msg} {suffix}\n")

## DEFINITIONS ##
def isVersion(p: str) -> bool:
    return isDir(p) and containsFile(p, "udr-document.json") and containsFile(p, "udr-archive.json")

def isLibrary(p: str) -> bool:
    return isDir(p) and containsFile(p, "library.json")

def isSchemaFile(p: str) -> bool:
    return isFile(p) and endsWith(p, ".json") and not startsWith(p, "_")

def isMainSchema(p: str) -> bool:
    return isSchemaFile(p) and (endsWith(p, "udr-document.json") or endsWith(p, "udr-archive.json"))

def isSubSchema(p: str) -> bool:
    return isSchemaFile(p) and not isMainSchema(p)

def generateLargeBanner(text):
    border = "+" + "-" * (len(text) + 2) + "+"
    return f"{border}\n| {text} |\n{border}"

def generateSmallBanner(text, width=50):
    padding_each_side = (width - len(text) - 4) // 2
    return f"[{'-'*padding_each_side} {text} {'-'*(width - len(text) - 2 - padding_each_side)}]"

## FUNCTIONS ##
def validateSchema(p: str) -> JSONSchema:
    print(f"Validating schema {path.basename(p)}...", end=" ")

    try:
        schema = JSONSchema.loadf(p)
        print(fg.GREEN + "SUCCESS")
        return schema
    except Exception as e:
        print(fg.RED + "FAILED")
        print(fg.RED + f"Error: {e}")
        incrementErrorCount();

def validateDocument(p: str, schema: JSONSchema): 
    print(f"Validating document {path.basename(p)}...", end=" ")

    try:
        document = JSON.loadf(p)
        result = schema.evaluate(document)

        isValid = result.output('flag')['valid']
        if not isValid:
            raise Exception(result.output('detailed'))
        print(fg.GREEN + "SUCCESS") 
        
    except Exception as e:
        print(fg.RED + "FAILED")
        if "errors" in JSON(e.args[0]):
            print(fg.RED + "Error: " + str(JSON(e.args[0])['errors'][0]['error']).strip('"'))
        else:
            print(fg.RED + "Error: " + str(e))
        incrementErrorCount()

    return document

def validateFailingDocument(p: str, schema: JSONSchema): 
    print(f"Validating document {path.basename(p)}...", end=" ")

    try:
        document = JSON.loadf(p)
        result = schema.evaluate(document)

        isValid = result.output('flag')['valid']
        if not isValid:
            raise Exception(result.output('detailed'))
        print(fg.RED + "FAILED")
        incrementErrorCount();
        
    except Exception as e:
        print(fg.GREEN + "SUCCESS")
        if "errors" in JSON(e.args[0]):
            print(fg.GREEN + str(JSON(e.args[0])['errors'][0]['error']).strip('"'))
        else:
            print(fg.GREEN + str(e))

    return document

def processSchemas(p: str):
    mainDocumentSchema = validateSchema(path.join(p, "udr-document.json"))
    mainArchiveSchema = validateSchema(path.join(p, "udr-archive.json"))

    subSchemas = filter(isSubSchema, getSubPaths(p))
    for subSchema in subSchemas:
        validateSchema(subSchema)
    
    return mainDocumentSchema, mainArchiveSchema

def processLibrary(p: str, mainDocumentSchema: JSONSchema):
    print("\n" + generateSmallBanner(f"Validating {path.basename(path.dirname(p))} library"))
    validateDocument(path.join(p, "library.json"), mainDocumentSchema)

    librarySubDirs = filter(isDir, getSubPaths(p))
    for librarySubDir in librarySubDirs:

        librarySubSchemas = filter(isSchemaFile, getSubPaths(librarySubDir))
        for librarySubSchema in librarySubSchemas:
            validateSchema(librarySubSchema)

def processTests(p: str, mainDocumentSchema: JSONSchema):
    shouldFailDir = path.join(p, "should_fail")
    shouldPassDir = path.join(p, "should_pass")

    if isDir(p) and (isDir(shouldFailDir) or isDir(shouldPassDir)):
        print("\n" + generateSmallBanner(f"Running tests"))

    if isDir(shouldFailDir):
        shouldFailDocs = filter(isSchemaFile, getSubPaths(shouldFailDir))
        for shouldFailDoc in shouldFailDocs:
            validateFailingDocument(shouldFailDoc, mainDocumentSchema)
    
    if isDir(shouldPassDir):
        shouldPassDocs = filter(isSchemaFile, getSubPaths(shouldPassDir))
        for shouldPassDoc in shouldPassDocs:
            validateDocument(shouldPassDoc, mainDocumentSchema)

def processVersion(p: str):
    currentVersion = path.basename(p)
    print("\n" + generateLargeBanner(f"Validating {currentVersion}") + "\n")

    catalog = create_catalog("2019-09")
    catalog._uri_sources[URI('esta:e173:')] = LocalSource(p, suffix='.json')

    mainDocumentSchema, _ = processSchemas(p)

    libraryDirs = filter(isLibrary, getLibrariesSubPaths(librariesPath, currentVersion))
    for libraryDir in libraryDirs:
        processLibrary(libraryDir, mainDocumentSchema)

    testsDir = getTestsSubPath(testsPath, currentVersion)
    processTests(testsDir, mainDocumentSchema)

## MAIN ##
def main ():
    versions = filter(isVersion, getSubPaths(schemasPath))

    for version in versions:
        processVersion(version)

    printStatus(f"Validation succeeded", f"Validation failed with {errCount} errors", errCount == 0)
    exit(errCount)

if __name__ == "__main__":
    main()